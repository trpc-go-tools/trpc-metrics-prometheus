## 配置
```yaml
plugins:                                          # 插件配置
  metrics:                                        # 引用metrics
    prometheus:                                   # 启动prometheus
      ip: 0.0.0.0                                 # prometheus绑定地址
      port: 8090                                  # prometheus绑定端口
      path: /metrics                              # 可选, metrics路径; 默认 metrics路径
      namespace: Development                      # 可选, 命名空间; 默认使用框架配置命名空间
      subsystem: trpc                             # 可选, 子系统; 默认使用框架配置trpc_${app}_${server}
      rawmode:   false                            # 可选, 原始模式，不会对metrics的特殊字符进行转换; 默认false
      enablepush: false                           # 可选, 启用push模式，默认不启用
      gateway: http://localhost:9091              # 可选, prometheus gateway地址
      password: username:MyPassword               # 可选, 设置账号密码， 以冒号分割
      job: job                                    # 可选, job名称
      pushinterval: 1                             # 可选, push间隔，默认1s上报一次
```

## 教程
### 引入prometheus
在main.go中引用,并在yaml中配置好参数

```golang
import _ "github.com/zanlichard/trpc-go-tools/trpc-metrics-prometheus"
```

### 上报数据
trpc metrics使用指引[trpc metrics](https://github.com/trpc-go/trpc-go/blob/master/metrics/README.md)

## 查询上报数据
本地通过curl查询指标，查看指标是否生成成功
```bash
curl ${ip}:${port}/$path |grep ${namespace}_${subsystem}
```

## 上报调用数据
增加配置
```yaml
  filter:
    - prometheus                                   #增加prometheus filter
```
调用数据目前支持请求耗时的Histogram和请求量的SUM两个指标
指标名前缀为ClientFilter与ServerFilter


## 注意事项
1. prometheus目前只支持PolicySUM/PolicySET/PolicyHistogram类型上报，其它类型支持请提pr
2. prometheus指标不支持中文与特殊字符，非法字符将会自动转换，acsii表内的非法字符转换为'_'，中文等utf8字符转换为对应的数据，比如"trpc.中文指标"->"trpc_20013_25991_25351_26631_",关闭此功能可以使用设置rawmode为true，异常上报将直接失败
3. 插件只提供exporter，不提供平台与对接
4. 多维度上报使用 metrics.NewMultiDimensionMetricsX 接口设置多维度名，否则可能会出现冲突
5. 如果需要推送自定义数据，可以在插件初始化完之后调用GetDefaultPusher方法，否则返回的pusher为空
