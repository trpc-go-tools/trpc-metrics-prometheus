package prometheus

import (
	runtime "gitee.com/zeuslichard/trpc-metrics-runtime"
)

// Version prometheus version
const Version = "v0.1.5"

func init() {
	go runtime.StatReport(Version, "prometheus")
}
