package prometheus

import (
	"sync"
)

// metricsCache 指标缓存
type metricsCache struct {
	locker sync.RWMutex
	cache  map[string]interface{}
}

// NewMetricsCache 创建缓存
func NewMetricsCache() *metricsCache {
	return &metricsCache{
		locker: sync.RWMutex{},
		cache:  make(map[string]interface{}),
	}
}

// 创建指标的方法
type createMetricFunc func() interface{}

// Loader 从缓存加载指标，如果缓存不存在则执行创建方法进行创建
func (mc *metricsCache) Loader(key string, f createMetricFunc) interface{} {
	mc.locker.RLock()
	if v, ok := mc.cache[key]; ok {
		mc.locker.RUnlock()
		return v
	}
	mc.locker.RUnlock()

	mc.locker.Lock()
	if v, ok := mc.cache[key]; ok {
		mc.locker.Unlock()
		return v
	}

	// 执行创建函数
	v := f()
	mc.cache[key] = v
	mc.locker.Unlock()

	return v
}
