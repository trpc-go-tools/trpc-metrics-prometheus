package prometheus

import (
	"context"
	"errors"
	"net"
	"testing"

	"trpc.group/trpc-go/trpc-go/codec"
	"trpc.group/trpc-go/trpc-go/errs"
	"trpc.group/trpc-go/trpc-go/filter"
	"github.com/stretchr/testify/assert"
)

// TestFilter 主调和被调上报单测
func TestFilter(t *testing.T) {
	setup(t)
	type ExampleStruct struct {
		body string
	}
	ctx, msg := codec.WithNewMessage(context.Background())
	msg.WithCalleeApp("App")
	msg.WithCalleeServer("Server")
	msg.WithCalleeService("Service")
	msg.WithCalleeMethod("Method")
	msg.WithCallerServer("CallerApp")
	msg.WithCallerApp("CallerApp")
	msg.WithCallerServer("CallerServer")
	msg.WithCallerService("CallerService")
	msg.WithCallerMethod("CallerMethod")
	msg.WithCalleeContainerName("ContainerName")
	msg.WithCalleeSetName("SetName")
	testIP := &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 80}
	t.Log(testIP.String())
	msg.WithRemoteAddr(testIP)
	msg.WithLocalAddr(testIP)

	f := func(ctx context.Context, req interface{}, rsp interface{}) (err error) {
		return nil
	}
	req := &ExampleStruct{
		body: "req",
	}
	rsp := &ExampleStruct{}
	filterReport(t, ctx, req, rsp, f)
	t.Log(getMetrics(t))

	f = func(ctx context.Context, req interface{}, rsp interface{}) (err error) {
		return errs.ErrServerTimeout
	}
	filterReportErr(t, ctx, req, rsp, f)
	t.Log(getMetrics(t))

	f = func(ctx context.Context, req interface{}, rsp interface{}) (err error) {
		return errors.New("myerr")
	}
	filterReportErr(t, ctx, req, rsp, f)
	t.Log(getMetrics(t))

	f2 := func(ctx context.Context, req interface{}) (rsp interface{}, err error) {
		return rsp, nil
	}
	newFilterReport(t, ctx, req, f2)
	t.Log(getMetrics(t))

	f2 = func(ctx context.Context, req interface{}) (rsp interface{}, err error) {
		return rsp, errs.ErrServerTimeout
	}
	newFilterReportErr(t, ctx, req, f2)
	t.Log(getMetrics(t))

}

func filterReport(t *testing.T, ctx context.Context, req, rsp interface{}, f filter.HandleFunc) {
	//case1:主调上报
	err := ClientFilter(ctx, req, rsp, f)
	assert.Nil(t, err)

	//case2:被调上报
	err = OldServerFilter(ctx, req, rsp, f)
	assert.Nil(t, err)
}
func filterReportErr(t *testing.T, ctx context.Context, req, rsp interface{}, f filter.HandleFunc) {
	//case1:主调上报
	err := ClientFilter(ctx, req, rsp, f)
	assert.NotNil(t, err)

	//case2:被调上报
	err = OldServerFilter(ctx, req, rsp, f)
	assert.NotNil(t, err)
}

func newFilterReport(t *testing.T, ctx context.Context, req interface{}, f filter.ServerHandleFunc) {
	//case2:被调上报
	_, err := ServerFilter(ctx, req, f)
	assert.Nil(t, err)
}
func newFilterReportErr(t *testing.T, ctx context.Context, req interface{}, f filter.ServerHandleFunc) {
	//case2:被调上报
	_, err := ServerFilter(ctx, req, f)
	assert.NotNil(t, err)
}
