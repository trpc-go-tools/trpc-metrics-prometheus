package prometheus

import (
	"strings"

	"trpc.group/trpc-go/trpc-go"
	"trpc.group/trpc-go/trpc-go/filter"
	"trpc.group/trpc-go/trpc-go/log"
	"trpc.group/trpc-go/trpc-go/plugin"
)

const (
	pluginType = "metrics"
	pluginName = "prometheus"
)

func init() {
	//register plugin
	plugin.Register(pluginName, &Plugin{})
	//register filter
	filter.Register(pluginName, ServerFilter, ClientFilter)
}

// Config 配置
type Config struct {
	IP           string `yaml:"ip"`           //metrics监控地址
	Port         int32  `yaml:"port"`         //metrics监听端口
	Path         string `yaml:"path"`         //metrics路径
	Namespace    string `yaml:"namespace"`    //正式或者测试
	Subsystem    string `yaml:"subsystem"`    //默认trpc
	RawMode      bool   `yaml:"rawmode"`      //默认将会对metrics特殊字符进行转换
	EnablePush   bool   `yaml:"enablepush"`   //默认不启用push的方式
	Password     string `yaml:"password"`     // 账号密码
	Gateway      string `yaml:"gateway"`      //push gateway地址
	PushInterval uint32 `yaml:"pushinterval"` //推送间隔,默认1s
	Job          string `yaml:"job"`          //上报任务名称
}

// Default set default values
func (c Config) Default() *Config {
	return &Config{
		IP:           "127.0.0.1",
		Port:         8080,
		Path:         "/metrics",
		Namespace:    "Development",
		Subsystem:    "trpc",
		RawMode:      false,
		EnablePush:   false,
		Gateway:      "",
		PushInterval: 1,
		Job:          "",
	}
}

// Plugin plugin obj
type Plugin struct {
}

// Type plugin type
func (p *Plugin) Type() string {
	return pluginType
}

// Setup init plugin
func (p *Plugin) Setup(name string, decoder plugin.Decoder) error {

	cfg := Config{}.Default()
	cfg.Namespace = trpc.GlobalConfig().Global.Namespace
	cfg.Subsystem = "trpc_" + trpc.GlobalConfig().Server.App + "_" + trpc.GlobalConfig().Server.Server

	err := decoder.Decode(cfg)
	if err != nil {
		log.Errorf("trpc-metrics-prometheus:conf Decode error:%v", err)
		return err
	}
	go func() {
		err := initMetrics(cfg.IP, cfg.Port, cfg.Path)
		if err != nil {
			log.Errorf("trpc-metrics-prometheus:running:%v", err)
		}
	}()
	initSink(cfg)

	return nil
}

func basicAuthForPasswordOption(s string) (username, password string) {
	splits := strings.Split(s, ":")
	if len(splits) < 2 {
		return
	}

	return splits[0], splits[1]
}
