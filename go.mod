module gitee.com/trpc-go-tools/trpc-metrics-prometheus

go 1.14

require (
	gitee.com/zeuslichard/trpc-metrics-runtime v1.0.0
	github.com/prometheus/client_golang v1.9.0
	github.com/prometheus/common v0.18.0
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/stretchr/testify v1.8.0
	go.uber.org/automaxprocs v1.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1
	trpc.group/trpc-go/trpc-go v1.0.1
)
