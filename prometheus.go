package prometheus

import (
	"fmt"
	"net/http"
	"strconv"

	"trpc.group/trpc-go/trpc-go/log"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/model"
)

// initMetrics initialize metrics and metrics handler
func initMetrics(ip string, port int32, path string) error {
	metricsHTTPHandler := http.NewServeMux()
	metricsHTTPHandler.Handle(path, promhttp.Handler())
	addr := fmt.Sprintf("%s:%d", ip, port)
	server := &http.Server{
		Addr:    addr,
		Handler: metricsHTTPHandler,
	}
	log.Infof("prometheus exporter running at %s, metrics path %s", addr, path)
	return server.ListenAndServe()
}

// convertSpecialChars convert utf8 chars to _
func convertSpecialChars(in string) (out string) {
	if len(in) == 0 {
		return
	}
	for i, b := range []rune(in) {
		if !isNormalChar(i, b) {
			// use '_' instead of illegal character
			if b < 127 {
				out = out + "_"
			} else {
				//convert utf8 to int string
				out = out + strconv.Itoa(int(b)) + "_"
			}
			continue
		}
		out = out + string(b)
	}
	return
}

func isNum(b rune) bool {
	return b >= '0' && b <= '9'
}

func isChar(b rune) bool {
	return (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z')
}

func isNormalChar(i int, b rune) bool {
	return isChar(b) || b == '_' || b == ':' || (isNum(b) && i > 0)
}

// checkMetricsValid metrics only support ascii letters and digists
func checkMetricsValid(name string) bool {
	return model.IsValidMetricName(model.LabelValue(name))
}
