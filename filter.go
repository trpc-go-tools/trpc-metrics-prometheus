package prometheus

import (
	"context"
	"fmt"
	"strings"
	"time"

	"trpc.group/trpc-go/trpc-go"
	"trpc.group/trpc-go/trpc-go/codec"
	"trpc.group/trpc-go/trpc-go/errs"
	"trpc.group/trpc-go/trpc-go/filter"
	"trpc.group/trpc-go/trpc-go/metrics"
)

// 设置耗时区间
// 1 10 20 40 80 160 1000 2000 3000
var clientBounds = metrics.NewValueBounds(1.0, 10.0, 20.0, 40.0, 80.0, 160.0, 1000.0, 2000.0, 3000.0)
var serverBounds = clientBounds

// SetClientBounds set client filter buckets
func SetClientBounds(b metrics.BucketBounds) {
	clientBounds = b
}

// SetServerBounds set server filter buckets
func SetServerBounds(b metrics.BucketBounds) {
	clientBounds = b
}

func getLabels(msg codec.Msg, err error, isServer bool) []*metrics.Dimension {
	code := fmt.Sprintf("%d", errs.RetOK)
	if err != nil {
		e, ok := err.(*errs.Error)
		if ok && e != nil {
			if e.Desc != "" {
				code = fmt.Sprintf("%s_%d", e.Desc, e.Code)
			} else {
				code = fmt.Sprintf("%d", e.Code)
			}
		} else {
			code = fmt.Sprintf("%d", errs.RetUnknown)
		}
	}
	var remoteAddr, localAddr string
	if msg.RemoteAddr() != nil {
		remoteAddr = getAddr(msg.RemoteAddr().String())
	}
	if msg.LocalAddr() != nil {
		localAddr = getAddr(msg.LocalAddr().String())
	}
	// 主调容器名称, 作为服务端时是不知道的, 客户端就是自身
	callerContainer := trpc.GlobalConfig().Global.ContainerName
	if isServer {
		callerContainer = ""
	}

	return []*metrics.Dimension{
		// {Name: "Namespace", Value: trpc.GlobalConfig().Global.Namespace},
		{Name: "Env", Value: msg.EnvName()},
		{Name: "CallerName", Value: msg.CallerApp() + "." + msg.CallerServer()},
		{Name: "CallerService", Value: msg.CallerService()},
		{Name: "CallerMethod", Value: msg.CallerMethod()},
		{Name: "CallerContainerName", Value: callerContainer},
		{Name: "CalleeName", Value: msg.CalleeApp() + "." + msg.CalleeServer()},
		{Name: "CalleeService", Value: msg.CalleeService()},
		{Name: "CalleeMethod", Value: msg.CalleeMethod()},
		{Name: "CalleeContainerName", Value: msg.CalleeContainerName()},
		{Name: "RemoteAddr", Value: remoteAddr},
		{Name: "LocalAddr", Value: localAddr},
		{Name: "Code", Value: code},
	}
}

// ClientFilter client filter for prome
func ClientFilter(ctx context.Context, req, rsp interface{}, handler filter.HandleFunc) error {
	begin := time.Now()
	hErr := handler(ctx, req, rsp)
	msg := trpc.Message(ctx)
	labels := getLabels(msg, hErr, false)
	ms := make([]*metrics.Metrics, 0)
	t := float64(time.Since(begin)) / float64(time.Millisecond)
	ms = append(ms,
		metrics.NewMetrics("time", t, metrics.PolicyHistogram),
		metrics.NewMetrics("requests", 1.0, metrics.PolicySUM))
	metrics.Histogram("ClientFilter_time", clientBounds)
	r := metrics.NewMultiDimensionMetricsX("ClientFilter", labels, ms)
	_ = GetDefaultPrometheusSink().Report(r)
	return hErr
}

// OldServerFilter server filter for prome
func OldServerFilter(ctx context.Context, req, rsp interface{}, handler filter.HandleFunc) error {
	begin := time.Now()
	hErr := handler(ctx, req, rsp)
	msg := trpc.Message(ctx)
	labels := getLabels(msg, hErr, true)
	ms := make([]*metrics.Metrics, 0)
	t := float64(time.Since(begin)) / float64(time.Millisecond)
	ms = append(ms,
		metrics.NewMetrics("time", t, metrics.PolicyHistogram),
		metrics.NewMetrics("requests", 1.0, metrics.PolicySUM))
	metrics.Histogram("ServerFilter_time", serverBounds)
	r := metrics.NewMultiDimensionMetricsX("ServerFilter", labels, ms)
	_ = GetDefaultPrometheusSink().Report(r)
	return hErr
}

// ServerFilter server filter for prome
func ServerFilter(ctx context.Context, req interface{}, handler filter.ServerHandleFunc) (rsp interface{}, err error) {
	begin := time.Now()
	rsp, err = handler(ctx, req)
	msg := trpc.Message(ctx)
	labels := getLabels(msg, err, true)
	ms := make([]*metrics.Metrics, 0)
	t := float64(time.Since(begin)) / float64(time.Millisecond)
	ms = append(ms,
		metrics.NewMetrics("time", t, metrics.PolicyHistogram),
		metrics.NewMetrics("requests", 1.0, metrics.PolicySUM))
	metrics.Histogram("ServerFilter_time", serverBounds)
	r := metrics.NewMultiDimensionMetricsX("ServerFilter", labels, ms)
	_ = GetDefaultPrometheusSink().Report(r)
	return
}

// getAddr 获取IP，不包括端口
func getAddr(add string) string {
	var addr string
	s := strings.Split(addr, ":")
	if len(s) > 0 {
		addr = s[0]
	}
	return addr
}
